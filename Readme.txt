
This Playbook is validate the differances between OSPF and ISIS routing tables 
the project consists of 2 play-books

1- will collect inet.0 routing information from the target nodes
2- will do the anlyses and generate a comparison report in a text format that covers all the deffirances between OSPF and ISIS routes


Steps to run the play project 


1- update the hosts file located in the project folder with the IP addresses and credentials for the nodes to be tested 
2- run the collection play-book using this command "pb.collect.inet.0.information.yaml "
3- run the analyses play-book using this command "pb.vars.yaml "

4- a report in a text format will be created and saved in the reports folder in the project directory


